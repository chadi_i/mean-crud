import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ElementService {
 base_url="http://localhost:3000";

constructor(private http:HttpClient) { }

addElem(elem){
 
return this.http.post(this.base_url+"/addElem",elem);


 }
getAllElem(){
  return this.http.get(this.base_url+"/getAllElem")

}
deleteElem(id){
return this.http.delete(this.base_url+"/deleteElem/"+id)

}

getOne(id){
  return this.http.get(this.base_url+"/getOne/"+id);
}

updateElem(id,elemnt){

  return this.http.put(this.base_url+"/updateElem/"+id,elemnt);
}


























 //OBS(detecter tout les changement)-Promise (detecter marra barka)


//  obs =new Observable(obs=>{
//    console.log("OBS RESULT :");
   
//   obs.next("A");
//   obs.next("B");
//   obs.error("ERRRR");
//   obs.complete();
  
// })


// p=new Promise((resolve,reject)=>{
//   console.log("PROMISE RESULT :");
//   resolve("A");
//   setTimeout(()=>{
//     resolve("B")
//   },3000)
//   reject("errr")
// })


}