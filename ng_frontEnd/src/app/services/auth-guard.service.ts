import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private router:Router) { }

  canActivate(chadi,state:RouterStateSnapshot):boolean {
   
  const tkn =localStorage.getItem('token')
    if (tkn) {

      return true;
    }
   
      else {
        this.router.navigate([''])
  
        return false;

      }
          
     
   }
}
