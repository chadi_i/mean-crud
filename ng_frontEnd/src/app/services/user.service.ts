import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isAuthenticated = false;
  base_url="http://localhost:3000";
  constructor(private http: HttpClient) { }

  register(user){
    return this.http.post(this.base_url+'/signup',user)
  }


  login(user) {
    this.isAuthenticated = true;
    return this.http.post(this.base_url +'/login', user)
  }


  logout() {
    this.isAuthenticated = false;
    localStorage.clear()
  }
}
