import { ElementService } from './../../services/element.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subject } from 'rxjs';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.css']
})
export class ElementComponent implements OnInit {
  allElem
  addform: FormGroup


  indexToUpdate
  editMode :boolean=false;
  //refrech ds la meme page
  //  properties: [];
  //  propertiesSubject = new Subject<[]>(); 
  constructor(private srv: ElementService, 
    private fb: FormBuilder, 
    private toastr: ToastrService,
    private userService:UserService
    ,private router:Router) { }

  ngOnInit() {


    this.addform = this.fb.group({
      element_name: ['', Validators.required],

    })
    this.getAllElement()

  }


  insertOrUpdateElement(elem) {
 if(this.editMode==false){
  this.createElement()
  
 }
 else {
   if(this.indexToUpdate!=null){
   
 this.editElement()
 this.editMode=false
   }
  }
  }
   createElement(){
   
    this.srv.addElem(this.addform.value).subscribe((data) => {
      this.toastr.success("l'elemnt ajoutée " 
      +this.addform.value.element_name +
      " avec succes")

      this.addform.reset();
      this.getAllElement();


    })
  }





  editElement(){
    this.srv.updateElem(this.indexToUpdate,this.addform.value)
    .subscribe((data)=>{
      console.log("update : ",data);
      this.toastr.success("element_name :"+ this.addform.value.element_name+" modifiée avec succees :) .. ");
      this.addform.reset();
      this.getAllElement()
    })
  }

  indexElement(e:any) { 
    this.editMode=true;
    this.addform.get('element_name').setValue(e.element_name);
    this.indexToUpdate = e._id ; 
    console.log(this.indexToUpdate);
    
    
  }




  getAllElement() {
    this.srv.getAllElem().subscribe((data)=>{
      this.allElem=data
    })
  }



  deleteElem(id) {
    Swal.fire({
      title: 'bech tfas5ou ??',
      text: "l'element  va etre supprimé !! !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non'
    }).then((result) => {
      if (result.value) {
        console.log(result.value);


    this.srv.deleteElem(id).subscribe((data:any)=>{
this.getAllElement()

    })
    
        
        Swal.fire(
          "Supprimer!",
          "L element  a été supprimé",
          "success"
        );


      }
    }

    )
  }





  logout(){
this.userService.logout()
this.router.navigate(['/'])
  }
  
    




}
