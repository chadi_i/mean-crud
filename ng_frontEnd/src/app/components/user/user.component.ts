import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  formLogin:FormGroup
  signUpForm:FormGroup
  
  errMsgEmail
  errMsgPwd
  submitted:boolean
  rember:boolean

  decodedToken
  openRegister:Boolean=false
  constructor(private userService:UserService,private router:Router,private toastr: ToastrService,private fb:FormBuilder) { }

  ngOnInit() {

    this.formLogin = this.fb.group({
      email: ['', Validators.email],
      password:'',
     
    })

    this.signUpForm = this.fb.group({
        
      email: ['', Validators.email],
      password:'',
      fullName: '',
      
    
    })
  }


  login() {
    this.submitted = true;
    this.errMsgEmail=undefined;
    this.errMsgPwd=undefined;
   

    if (this.formLogin.valid) {
    this.userService.login(this.formLogin.value).subscribe(
    (data)=>{
    
   
    localStorage.setItem('token', JSON.stringify(data))
    this.router.navigate(['/home'])
  },
  
  dataError=>{
    console.log(dataError);
    
 this.errMsgEmail=dataError.error.errEmail;
 this.errMsgPwd=dataError.error.errPwd;

}
)

    } else
      this.formLogin.reset()


  }
  get email() { return this.formLogin.get('email'); }
  get password() { return this.formLogin.get('password'); }

  goToRegister(){

    this.openRegister=true;
  }

  
  register(){
    console.log("_signUp");
    let user = this.signUpForm.value;
    user.role = 'user'
    this.userService.register(user).subscribe((data)=>{
      this.toastr.success("Go to login now Mr :"+ this.signUpForm.value.fullName + " :) .. ");
      this.openRegister=false
      console.log(user);
      
     
    })
    
    
      }

      cancelL(){

        this.formLogin.reset()
      }
      cancelR(){

        this.signUpForm.reset();
        this.openRegister=false
      }
}
