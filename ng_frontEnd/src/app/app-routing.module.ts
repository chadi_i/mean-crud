

import { ElementComponent } from './components/element/element.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { AuthGuard } from './services/auth-guard.service';
import { Error404Component } from './components/error404/error404.component';


const routes: Routes = [

  {path:'',component:UserComponent},
  {path:'home',component:ElementComponent,canActivate:[AuthGuard]},
  {path:'**',component:Error404Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
