
const ELEMENT =require('../models/element.model');

exports.createElem=(req,res)=>{

    const elem =new ELEMENT(req.body);
    elem.save().then((result)=>{
        res.json(result);

    }
    )


}


exports.getAllElem = (req, res) => { 
    ELEMENT.find().then((result) => {res.json(result)})
}

exports.getElemById = (req, res) => { 
    ELEMENT.findOne({_id:req.params._id}).then((result) => {res.json(result)})
}


exports.deleteOneElem = (req,res) => {
    const id =req.params._id
    ELEMENT.deleteOne({_id:id})
    .then((result) => {
        res.send(true)
    })
     }

 

     exports.updateElem=(req, res) => {
        const id = req.params.id; //retrive  the  id 
        const conditions = { _id: id}; //put  id in  _id of  mongo
        const elem = {...req.body}; //req.body =(element_name,element_description..) 
        const update = { $set: elem }; //$set : update in  mongoDB
        const options = {
            upsert: true,//si property does not exist  => creation ,(upsert = insert in mongoDB)
            new: true   //return the modified property
           
        };
        ELEMENT.findOneAndUpdate(conditions,update, options, (err, response) => {
            if(err) return res.status(500).json({ msg: 'update failed', error: err });
            res.status(200).json({ msg: `document with id ${id} updated`, response: response });
        });
    }
 