const express = require('express');
const app = express();
//const router =express.Router();

const bodyParser=require('body-parser');
const cors =require('cors');
require('dotenv').config();
const mongoose = require('mongoose');


//middleware
app.use(cors());
app.use(bodyParser.json());

// db..when finish connecting..we get the promise "then"
mongoose
    .connect(process.env.DATABASE, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false ,
        useFindAndModify:false
    })
    .then(() => console.log('DB Connected'));




//Route middleware
//app.use('/',router)

require('./router/element.router')(app)
require('./router/user.router')(app)

const port = process.env.PORT || 3001;
app.listen(port,()=>{
    console.log(`server is running on port ${port}`);
})