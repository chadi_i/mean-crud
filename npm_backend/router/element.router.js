module.exports=(app)=>{

const elemController = require('../controllers/element.controller');
app.post('/addElem',elemController.createElem)
app.get('/getAllElem',elemController.getAllElem)
app.delete('/deleteElem/:_id',elemController.deleteOneElem)
app.get('/getOne/:_id',elemController.getElemById)
app.put('/updateElem/:id',elemController.updateElem)



}